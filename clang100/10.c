#include <stdio.h>

int main() {
	int a = 0;
	int b = 0;

	printf("Please input a and b: ");
	scanf("%d%d", &a, &b);

	if (a > b) {
		printf("a > b\n");
	} else { 
		printf("a <= b\n");
	}

	return 0;
}
