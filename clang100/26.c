#include <stdio.h>

// ѭ������׳�
long method_one(const int n) {
	long ret = 1;
	int i = 0;

	printf("====method_one====\n");
	for (i = 1; i <= n; i++) {
		ret = ret * i;
	}

	return ret;
}

// �ݹ�ʵ��
long method_two(const int n) {
	if (n >= 1) {
		// n!=(n-1)!��n��
		return method_two(n - 1) * n;
	}
	else {
		// 0!=1
		return 1;
	}
}

int main() {
	int n = 0;

	printf("Please n: ");
	scanf("%d", &n);
	
	printf("%d�Ľ׳ˣ�%ld\n", n, method_one(n));

	printf("====method_two====\n");
	printf("%d�Ľ׳ˣ�%ld\n", n, method_two(n));

	return 0;
}