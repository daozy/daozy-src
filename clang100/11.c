/*************************************************************************
	> File Name: 11.c
	> Author: WangTeacher
	> Mail: daozy@163.com 
	> Created Time: 2020年06月08日 星期一 06时38分08秒
 ************************************************************************/

#include<stdio.h>

int main() {
	// sizeof(int) = 4
	// u == unsigned == unsigned int
	printf("sizeof(char) = %lu\n", sizeof(char));
	// short int == short
	printf("sizeof(short) = %lu\n", sizeof(short));
	printf("sizeof(int) = %lu\n", sizeof(int));
	printf("sizeof(long) = %lu\n", sizeof(long));
	printf("sizeof(long long) = %lu\n", sizeof(long long));
	
	printf("sizeof(float) = %lu\n", sizeof(float));
	printf("sizeof(double) = %lu\n", sizeof(double));
	printf("sizeof(long double) = %lu\n", sizeof(long double));
	
	return 0;
}
