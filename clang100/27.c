#include <stdio.h>

int main() {
	char c = 'A';

	// ��� A -> Z
	printf("====A -> Z====\n");
	for (c = 'A'; c <= 'Z'; c++) {
		printf("%c ", c);
	}

	// ��� a -> z
	printf("\n====a -> z====\n");
	for (c = 'a'; c <= 'z'; c++) {
		printf("%c ", c);
	}

	return 0;
}