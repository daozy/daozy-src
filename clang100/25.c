#include <stdio.h>


void method_one(const unsigned m, const unsigned n) {
	unsigned max = m > n ? m : n;
	unsigned i = 0;
	printf("=====method_one=====\n");
	for (i = max; i < m * n; i++) {
		if (i % m == 0 && i % n == 0) {
			break;
		}
	}

	printf("最小公倍数: %u\n", i);
}

void method_two(const unsigned m, const unsigned n) {
	// 最小公倍数 = m*n / 最大公约数
	unsigned i = 0;
	// 最大公约数
	unsigned max = 1;

	printf("=====method_two=====\n");
	for (i = 1; i < m && i < n; i++) {
		if (m % i == 0 && n % i == 0) {
			max = i;
		}
	}

	printf("最大公倍数：%u\n", m * n / max);
}


int main() {
	unsigned m = 0;
	unsigned n = 0;

	printf("Please input m, n: ");
	scanf("%u%u", &m, &n);

	// 第一种方法:遍历法
	method_one(m, n);

	// 第二种方法：公式法
	method_two(m, n);

	return 0;
}