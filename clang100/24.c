#include <stdio.h>

int main() {
	unsigned m = 0;
	unsigned n = 0;
	unsigned i = 0;
	// 最大公约数
	unsigned x = 0;

	printf("Please input m, n: ");
	scanf("%d%d", &m, &n);

	// 1 ---> min(m, n)
	for (i = 1; i < m && i < n; i++) {
		if (m % i == 0 && n % i == 0) {
			x = i;
			printf("公约数: %u\n", x);
		}
	}

	printf("最大公约数: %u\n", x);

	return 0;
}