#include <stdio.h>

int is_prime(const int x) {
	int i = 0;
	// 1 表示是素数
	int flag = 1;
	
	for (i = 2; i <= x / 2; i++) {
		if (x % i == 0) {
			flag = 0;
			break;
		}
	}

	return flag;
}

// 找出指定范围内所有素数
int main() {
	int min = 0;
	int max = 0;
	int i = 0;

	printf("Please range(min, max): ");
	scanf("%d%d", &min, &max);

	for (i = min; i <= max; i++) {
		if (is_prime(i)) {
			printf("素数 %d\n", i);
		}
	}

	return 0;
}