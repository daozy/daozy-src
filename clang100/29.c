#include <stdio.h>

long long loop(const int x, const int n) {
	int i = 0;
	long long ret = 1;

	for (i = 0; i < n; i++) {
		ret = ret * x;
	}

	return ret;
}

long long power(const int x, const int n) {
	if (n != 0) {
		return x * power(x, n - 1);
	}
	else {
		return 1;
	}
}

int main() {
	int x = 0;
	int n = 0;

	printf("Please input x, n: ");
	scanf("%d%d", &x, &n);

	// 循环求解
	printf("====循环求解====\n");
	printf("%d^%d = %lld\n", x, n, loop(x, n));

	// 递归法
	printf("====递归求解====\n");
	printf("%d^%d = %lld\n", x, n, power(x, n));

	return 0;
}