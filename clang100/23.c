#include <stdio.h>

int main() {
	int i = 0;
	int n = 0;
	int t = 0;
	int t1 = 0;
	int t2 = 0;

	printf("Please input n(>=2): ");
	scanf("%d", &n);
	if (n < 2) {
		printf("Input error: %d\n", n);
		return -1;
	}

	n = n - 2;
	printf("1 1");
	t1 = 1;
	t2 = 1;
	
	// 1(t1), 1(t2), 2, 3, 5
	for (i = 0; i < n; i++) {
		t = t1 + t2; // 2 = 1 + 1
		printf(" %d", t);
		// 前面的两个数向后移动一位
		t1 = t2;
		t2 = t;
	}

	printf("\n");

	return 0;
}