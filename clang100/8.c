#include <stdio.h>

int main() {
	char c = '\0';

	printf("Please input a char: ");
	scanf("%c", &c);

	printf("%c -> ASCII: %d\n", c, c);

	return 0;
}
