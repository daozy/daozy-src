/*************************************************************************
	> File Name: 12.c
	> Author: WangTeacher
	> Mail: daozy@163.com 
	> Created Time: 2020年06月09日 星期二 07时04分09秒
 ************************************************************************/

#include<stdio.h>

void swap1(int* x, int* y);
void swap2(int* x, int* y);
void swap3(int* x, int* y);

int main() {
	int a = 0;
	int b = 0;

	printf("Input two int(a,b): ");
	scanf("%d%d", &a, &b);
	printf("a = %d, b = %d\n", a, b);
	
	// 函数是通过值传递参数
	// swap1(a, b);
	// 传如的地址
	// swap1(&a, &b);
	// swap2(&a, &b);
	swap3(&a, &b);

	printf("a = %d, b = %d\n", a, b);

	return 0;
}

// 形式参数
void swap1(int* x, int* y) {
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

void swap2(int* a, int* b) {
	*a = *a + *b;
	*b = *a - *b;
	*a = *a - *b;
}

void swap3(int* a, int* b) {
	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}
