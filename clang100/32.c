#include <stdio.h>
#include <math.h>

int ams(const int x) {
	int t = x;
	int m = 0;
	int val[128] = { 0 };
	int l = 0;
	int i = 0;
	long sum = 0;

	// 153
	// 15 % 10
	// 第一步：取数字
	while (t != 0) {
		val[l++] = t % 10;
		t /= 10; // t = 15
	}

	// 第二步：求和
	// val[3] = {3, 5, 1}
	for (i = 0; i < l; i++) {
		sum += (long)pow(val[i], l);
	}

	// 第三步：比较
	return sum == x;
}

int main() {
	int min = 0;
	int max = 0;
	int i = 0;

	printf("Please range(min, max): ");
	scanf("%d%d", &min, &max);

	for (i = min; i <= max; i++) {
		if (ams(i)) {
			printf("%d 是\n", i);
		}
	}

	return 0;
}