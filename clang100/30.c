#include <stdio.h>

void method1(const int x) {
	int x1 = 0;
	int m = 0;
	int n = x;

	printf("====mehtod1====\n");
	// x = 123
	// 1. x1 = 3
	// 2. x1 = 32
	// 3. x1 = 321
	while (n != 0) {
		m = n % 10; // x = 123, m = 3; x = 12, m = 2
		x1 = x1 * 10 + m; // x1 = 0 + 3 = 3; x1 = 30 + 2 = 32
		n = n / 10;  // x = 12, x = 1
	}

	// x1 = 321, x == 123
	if (x == x1) {
		printf("%d是回文数\n", x);
	}
	else {
		printf("%d不是回文数\n", x);
	}
}

void method2(const int x) {
	char s[128] = { 0 };
	char s1[128] = { 0 };
	int i = 0;
	int j = 0;

	printf("====mehtod2====\n");
	sprintf(s, "%d", x);
	// 字符串翻转
	// s = 123
	// s1 = 321
	for (i = strlen(s) - 1; i >= 0; i--) {
		s1[j++] = s[i];
	}

	if (strcmp(s, s1) == 0) {
		printf("%d是回文数\n", x);
	}
	else {
		printf("%d不是回文数\n", x);
	}
}

int main() {
	int x = 0;

	printf("Please x: ");
	scanf("%d", &x);

	// 方法1：翻转数，然后比较数值
	method1(x);

	// 方法2: 字符串比较
	method2(x);

	return 0;
}