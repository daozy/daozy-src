#include <stdio.h>
#include <math.h>

int main(int argc, char* argv[]) {
    const double pi = 3.1415926;
    double r = 0.0;
    double h = 0.0;
    double area = 0.0;

    scanf("%lf%lf", &r, &h);
    area = pi * r * r * 2 + 2 * pi * r * h;
    
    printf("area = %10.3lf\n", area);

    return 0;
}
