#include <stdio.h>

#define MAX_SIZE 100
#define STAT_MAX_SIZE 127

typedef unsigned int uint32;

int main() {
	char tmp = 0;
	char char_set[MAX_SIZE] = {0};
	uint32 i = 0;
	uint32 stat[STAT_MAX_SIZE] = {0};

	printf("please input chars in line: ");

	while (scanf("%c", &tmp) && tmp != '\n') {
		char_set[i++] = tmp;
	}

	for (i = 0; i < MAX_SIZE && char_set[i] != 0; i++) {
		printf("%c", char_set[i]);
		stat[char_set[i]]++;
	}

	for (i = 0; i < STAT_MAX_SIZE; i++) {
		if (stat[i] == 0) {
			continue;
		}

		printf("%c: %u\n", i, stat[i]);
	}

	return 0;
}
