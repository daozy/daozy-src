#include <stdio.h>

int main() {
	char x = 'a';

	while (x <= 'z') {
		printf("%d ", x++);
	}

	printf("\n=================\n");

	x = 'A';
	while (x <= 'Z') {
		printf("%d ", x++);
	}

	printf("\n=================\n");

	x = '0';
	while (x <= '9') {
		printf("%d ", x++);
	}

	return 0;
}
