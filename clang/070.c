#include <stdio.h>
#include <malloc.h>

typedef struct _Node {
	int v;
	struct _Node* next;
} Node;

Node* header = NULL;

void init() {
	int i = 0;
	Node* node = NULL;
	Node* new_node = NULL;

	for (i = 0; i < 10; i++) {
		if (header == NULL) {
			header = (Node*)malloc(sizeof(Node));
			header->v = i;
			header->next = NULL;
			node = header;
		}
		else {
			new_node = (Node*)malloc(sizeof(Node));
			new_node->v = i;
			new_node->next = NULL;
			node->next = new_node;
			node = new_node;
		}
	}
}

void output_list() {
	Node* node = header;

	while (node != NULL) {
		printf("node->v = %d\n", node->v);
		node = node->next;
	}
}

void remove_list(const int x) {
	Node* node = header;
	Node* pre = NULL;
	Node* next = NULL;

	while (node != NULL) {
		if (node->v == x) {
			if (pre) {
				pre->next = node->next;
			}
			else {
				header = node->next;
			}

			printf("remove node->v = %d\n", node->v);
			next = node->next;
			free(node);
			node = next;
		}
		else {
			pre = node;
			node = node->next;
		}
	}
}

int main() {
	int x = 0;

	init();
	output_list();

	printf("Please input remove value: ");
	while (scanf("%d", &x)) {
		remove_list(x);
		output_list();
		printf("Please input remove value: ");
	}

	return 0;
}