#include <stdio.h>
#include <string.h>

int main() {
    char str1[11] = { 0 }; // 只能存9个字符，最后一个是结束符'\0'
    char* str2 = "0123456789";
    
    printf("str1 = %s\n", str1);
    strcpy(str1, str2);
    printf("str1 = %s\n", str1);

    return 0;
}