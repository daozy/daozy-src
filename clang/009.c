#include <stdio.h>
#include <string.h>

void first_kind(char* in_color) {
	enum colour {
		white,
		black,
		blue,
		red,
		other
	};

	enum colour my_color = white;
	
	if (strcmp(in_color, "white") == 0) {
		my_color = white;
	} else if (strcmp(in_color, "black") == 0) {
		my_color = black;
	} else if (strcmp(in_color, "blue") == 0) {
		my_color = blue;
	} else if (strcmp(in_color, "red") == 0) {
		my_color = red;
	} else {
		my_color = other;
	}

	printf("first >> %s: %d\n", in_color, my_color);
}

void second_kind(char* in_color) {
	enum colour {
		white = 1,
		black,
		blue,
		red,
		other
	} my_color;

	if (strcmp(in_color, "white") == 0) {
		my_color = white;
	} else if (strcmp(in_color, "black") == 0) {
		my_color = black;
	} else if (strcmp(in_color, "blue") == 0) {
		my_color = blue;
	} else if (strcmp(in_color, "red") == 0) {
		my_color = red;
	} else {
		my_color = other;
	}

	printf("second >> %s: %d\n", in_color, my_color);
}

void third_kind(char* in_color) {
	enum {
		white = 2,
		black = 4,
		blue = 8,
		red = 16,
		other = 32
	} my_color;

	if (strcmp(in_color, "white") == 0) {
		my_color = white;
	} else if (strcmp(in_color, "black") == 0) {
		my_color = black;
	} else if (strcmp(in_color, "blue") == 0) {
		my_color = blue;
	} else if (strcmp(in_color, "red") == 0) {
		my_color = red;
	} else {
		my_color = other;
	}

	printf("third >> %s: %d\n", in_color, my_color);
}

void fourth_kind(char* in_color) {
	typedef enum {
		white,
		black,
		blue,
		red,
		other
	} colour;

	colour my_color = white;
	
	if (strcmp(in_color, "white") == 0) {
		my_color = white;
	} else if (strcmp(in_color, "black") == 0) {
		my_color = black;
	} else if (strcmp(in_color, "blue") == 0) {
		my_color = blue;
	} else if (strcmp(in_color, "red") == 0) {
		my_color = red;
	} else {
		my_color = other;
	}

	printf("fourth >> %s: %d\n", in_color, my_color);
}

int main() {
	char in_color[16] = {0};

	printf("please input your like color: ");
	scanf("%s", in_color);

	first_kind(in_color);
	second_kind(in_color);
	third_kind(in_color);
	fourth_kind(in_color);

	return 0;
}
