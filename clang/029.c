#include <stdio.h>
#include <string.h>

typedef struct {
	unsigned chinese;
	unsigned math;
	unsigned english;
} Score;

typedef struct {
	char name[32];
	char sex;
	unsigned age;
	char class[32];
	Score score;
} Student;


void print_info(const Student stud) {
	printf("name = %s, sex = %c, age = %u, class = %s\n", 
			stud.name, stud.sex, stud.age, stud.class);
	printf("score >> Chinese = %u, math = %u, English = %u\n",
			stud.score.chinese, stud.score.math, stud.score.english);
}

int main() {
	// 1: {}
	Student stud1 = {};
	// 2
	Student stud2 = {"stud2", 'n', 12, "二班", {99,88,77}};
	// 3
	Student stud3;
	// 4
	Student stud4 = {
		.score.chinese = 99,
		.score.math = 100,
		.score.english = 60,
		.class = "三班",
		.age = 13,
		.sex = 'v',
		.name = "stud4"
	};
	// 5	
	Student stud5 = {
		class : "三班",
		age : 13,
		sex : 'v',
		name : "stud4"
	};

	Student stud[10] = {};
	unsigned n = 0;
	int i = 0;

	printf("Please input number: ");
	scanf("%u", &n);
	printf("Please input info: ");
	for (i = 0; i < n; i++) {
		scanf("%s %c%u%s%u%u%u", stud[i].name, &stud[i].sex,
				&stud[i].age, stud[i].class, &stud[i].score.chinese,
				&stud[i].score.math, &stud[i].score.english);
	}

	for (i = 0; i < n; i++) {
		print_info(stud[i]);
	}

	printf("--------------------\n");
	
	for (i = 0; i < n; i++) {
		memset(&stud[i].score, 0, sizeof(Score));
		// stud[i].score.chinese = 0;
		// stud[i].score.math = 0;
		// stud[i].score.english = 0;
	}
	
	for (i = 0; i < n; i++) {
		print_info(stud[i]);
	}

	return 0;
}
