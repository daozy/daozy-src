#include <stdio.h>

void kind_one(const int a, const int b);
void kind_two(const int a, const int b);

int main() {
	int a = 0;
	int b = 0;

	printf("Please input a and b: ");
	scanf("%d%d", &a, &b);

	kind_one(a, b);
	printf("--------------------\n");
	kind_two(a, b);

	
	return 0;
}

void kind_one(const int a, const int b) {
	if (a == b) {
		printf("a == b ? true\n");
	} else {
		printf("a == b ? false\n");
	}

	if (a != b) {
		printf("a != b ? true\n");
	} else {
		printf("a != b ? false\n");
	}

	if (a > b) {
		printf("a > b ? true\n");
	} else {
		printf("a > b ? false\n");
	}
	
	if (a < b) {
		printf("a < b ? true\n");
	} else {
		printf("a <  b ? false\n");
	}
	
	if (a >= b) {
		printf("a >= b ? true\n");
	} else {
		printf("a >=  b ? false\n");
	}
	
	if (a <= b) {
		printf("a <= b ? true\n");
	} else {
		printf("a <=  b ? false\n");
	}
}

void kind_two(const int a, const int b) {
	printf("a == b ? %d\n", a == b);
	printf("a != b ? %d\n", a != b);
	printf("a > b ? %d\n", a > b);
	printf("a < b ? %d\n", a < b);
	printf("a >= b ? %d\n", a >= b);
	printf("a <= b ? %d\n", a <= b);
}
