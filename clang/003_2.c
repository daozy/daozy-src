#include <stdio.h>
#include <string.h>

int main() {
    char c = 0;
    char word[100] = {0};
    char line[200] = {0};

    scanf("%c\n%s\n%[^\n]%*c", &c, word, line);
    // scanf("%[^\n]%*c", word);
    // scanf("%[^\n]%*c", line);

    printf("%c\n%s\n%s\n", c, word, line);
    // printf("%s\n", word);
    // printf("%s\n", line);

    return 0;
}
