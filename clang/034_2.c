/*************************************************************************
	> File Name: 034_1.c
	> Author: WangTeacher
	> Mail: daozy@163.com 
	> Created Time: 2020年06月07日 星期日 07时53分48秒
 ************************************************************************/

#include<stdio.h>
int main() {
  char c = 'A';
  char *p = &c;
  
  printf("p = %p, &c = %p\n", p, &c);
  printf("c = %c, *p = %c\n", c, *p);
  *p = 'a';
  printf("c = %c, *p = %c\n", c, *p);
  
  return 0;
}
