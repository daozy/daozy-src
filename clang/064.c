#include <stdio.h>
#include <stdarg.h>

// 固定参数列表
int avg1(int a, int b, int c, int d) {
	return (a + b + c + c) / 4;
}

// 数组
int avg2(int a[], const size) {
	int i = 0;
	int sum = 0;

	for (i = 0; i < size; i++) {
		sum += a[i];
	}

	return sum / size;
}

// 可变参数列表
int avg3(int num, ...) {
	va_list valist;
	int i = 0;
	int sum = 0;

	// 为参数a初始化valist
	va_start(valist, num);

	// 访问所有在valist中的参数
	// 可变参数的类型需要一致
	for (i = 0; i < num; i++) {
		sum += va_arg(valist, int);
	}

	va_end(valist);

	return sum / num;
}

// 求一系列整数的平均数
int main() {
	int a[] = { 1, 2, 3, 4, 5 };
	printf("avg1: %d\n", avg1(1, 2, 3, 4));
	printf("avg2: %d\n", avg2(a, sizeof(a)/sizeof(int)));
	printf("avg3: %d\n", avg3(5, 1, 2, 3, 4, 5));
	printf("avg3: %d\n", avg3(6, 1, 2, 3, 4, 5, 100));

	return 0;
}