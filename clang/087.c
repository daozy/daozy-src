#include <stdio.h>
//#pragma pack(8)  64λ=8byte, 32λ=4byte

#pragma pack(1)

struct st1 {  // 12
    char  a;  // 1
    int   b;  // 4   min(pack(8), 4) = 4
    short c;  // 2   min(pack(8), 2) = 2
    // min(pack(8), 4) = 4
};

struct st2 {  // 8
    short c;  // 2  off = 0
    char  a;  // 1  off = 3
    int   b;  // 4  off = 4
};

int main() {
    printf("sizeof(st1) = %d\n", sizeof(struct st1));
    printf("sizeof(st2) = %d\n", sizeof(struct st2));

    return 0;
}