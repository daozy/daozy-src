// b.c
#include <stdio.h>

static int add(const int a, const int b) {
	return a + b;
}

int sum(const int input[], const int len) {
	int sum = 0;
	int i = 0;

	for (i = 1; i < len; i++) {
		sum = add(sum, input[i]);
	}

	return sum;
}
