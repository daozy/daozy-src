#include <stdio.h>

int main() {
	char x = '\0';
	int diff = 'a' - 'A';

	printf("please input a char: ");
	scanf("%c", &x);

	if (x >= 'A' && x <= 'Z') {
		printf("%c\n", x + diff);
	} else if (x >= 'a' && x <= 'z') {
		printf("%c\n", x - diff);
	} else {
		printf("%c\n", x);
	}

	return 0;
}
