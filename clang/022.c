#include <stdio.h>

int main() {
	int a = 0;
	int b = 0;

	scanf("%d%d", &a, &b);
	
	printf("a + b = %d\n", a + b);
	printf("a - b = %d\n", a - b);
	printf("a / b = %lf\n", (double)a / b);
	printf("a %% b = %d\n", a % b);
	printf("a++ = %d, b++ = %d\n", a++, b++);
	printf("a = %d\n", a);
	printf("a-- = %d, b-- = %d\n", a--, b--);
	

	a = 1;
	b = 2;
	printf("++a = %d, ++b = %d\n", ++a, ++b);
	printf("--a = %d, --b = %d\n", --a, --b);

	return 0;
}
