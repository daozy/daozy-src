#include <stdio.h>
#include <stdlib.h>
#include <math.h>

const double EPSINON = 0.00001;

int main() {
    double d1, d2, d3, d4;

    d1 = 194268.02;
    d2 = 194268;
    d4 = 0.02;
    d3 = d1 - d2;

    if (fabs(d3 - d4) < EPSINON) {
        printf("d3 == d4\n");
    }
    else {
        printf("d3 != d4\n");
    }

    printf("d3=%lf, d4=%lf\n", d3, d4);

    return 0;
}
