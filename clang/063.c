#include <stdio.h>

long int multipl(int n) {
	// 0!=1��n!=(n-1)!��n��
	if (n >= 1) {
		printf("n = %d\n", n);
		return multipl(n - 1) * n;
	}
	else {
		return 1;
	}
}

int main() {
	int n = 0;

	printf("Please input n: \n");
	scanf("%d", &n);

	printf("%d! = %ld\n", n, multipl(n));

	return 0;
}