#include <stdio.h>

int main() {
	unsigned int score[2][3] = {0};
	double avg[3] = {0};
	int j = 0;
	int i = 0;

	for (j = 0; j < 2; j++) {
		for (i = 0; i < 3; i++) {
			scanf("%u", &score[j][i]);	
		}
	}

	for (i = 0; i < 3; i++) {
		for (j = 0; j < 2; j++) {
			avg[i] += score[j][i];
		}
	}

	for (i = 0; i < 3; i++) {
		printf("%f ", avg[i]/2);
	}

	printf("\n");

	return 0;
}
