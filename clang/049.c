#include <stdio.h>

int main() {
	int a = 12345;
	int x[100] = { 0 };
	int i = 0;
	int j = 0;

	while (1) {
		x[i++] = a % 16;
		a = a / 16;
		if (a == 0) {
			break;
		}
	}

	for (j = 0; j < i; j++) {
		printf("%d ", x[j]);
	}

	printf("\n");

	return 0;
}