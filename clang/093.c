#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 这里是值传递，不是地址传递
void getmemory(char** p) {
	*p = (char*)malloc(100);
}

int main() {
	char* str = NULL;

	getmemory(&str);
	strcpy(str, "hello world");
	printf("%s\n", str);
	free(str);

	return 0;
}