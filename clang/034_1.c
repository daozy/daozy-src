/*************************************************************************
	> File Name: 034.c
	> Author: WangTeacher
	> Mail: daozy@163.com 
	> Created Time: 2020年06月03日 星期三 08时02分16秒
 ************************************************************************/

#include<stdio.h>

int *ptr;

void fun_ptr() {
	int *ptr1;
	int *ptr2 = 0xFFFFF;
	int a = 10;
	int *ptr3 = &a;


	printf("ptr = %p\n", ptr);
	printf("ptr1 = %p\n", ptr1);
	printf("ptr2 = %p\n", ptr2);
	printf("ptr3 = %p\n", ptr3);
	//printf("*ptr = %d\n", *ptr);
	//printf("*ptr1 = %d\n", *ptr1);
	//printf("*ptr2 = %d\n", *ptr2);
	printf("*ptr3 = %d\n", *ptr3);
}


void is_null_ptr(int *p) {
	if (p) {
		printf("p is not null\n");
	} else {
		printf("p is null\n");
	}
}

void char_ptr() {
	char *p = NULL;
	char c = 'A';
	int a = 10;
	int *pa = &a;

	printf("sizeof(p) = %ld, sizeof(pa) = %ld\n", sizeof(p), sizeof(pa));

	p = &c;
	p = (char*)&a;
}

int main() {
	int a = 0;
	int *p = NULL;

	char_ptr();

	fun_ptr();
	
	is_null_ptr(p);
	p = &a;
	is_null_ptr(p);

	printf("a = %d\n", a);
	
	printf("Input a: ");
	scanf("%d", &a);

	printf("*p = %d\n", *p);

	printf("Input a: ");
	scanf("%d", p);
	printf("a = %d\n", a);


	return 0;
}
