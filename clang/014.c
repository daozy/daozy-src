#include <stdio.h>
#include <string.h>

int main() {
	char s1[128] = {'\0'};
	char s2[128] = {'\0'};
	char ch = 0;
	char *p1 = NULL;
	char *p2 = NULL;

	scanf("%s", s1);
	scanf("%s", s2);

	printf("start >> s1: %s, s2: %s\n", s1, s2);
	
	// strcpy(s1, s2);
	// printf("end >> s1: %s, s2: %s\n", s1, s2);
	// printf("s1[3]: %c, s1[4]: %c\n", s1[3], s1[4]);
	
	//strcat(s1, s2);
	//printf("end >> s1: %s, s2: %s\n", s1, s2);
	
	//printf("s1.length: %lu, s2.length: %lu\n", strlen(s1), strlen(s2));

	/*	
	if (strcmp(s1, s2) == 0) {
		printf("s1 == s2\n");
	} else if (strcmp(s1, s2) > 0) {
		printf("s1 > s2\n");
	} else {
		printf("s1 < s2\n");
	}
	*/
	
	/*
	ch = getchar();
	ch = getchar();
	p1 = strchr(s1, ch);
	p2 = strchr(s2, ch);
	
	if (p1) {
		printf("p1 >> %p: %c\n", p1, *p1);
	} else {
		printf("p1 >> address: %p\n", p1);
	}

	if (p2) {
		printf("p2 >> %p: %c\n", p2, *p2);
	} else {
		printf("p2 >> address: %p\n", p2);
	}
	*/
	
	p1 = strstr(s1, s2);
	printf("p1: %s\n", p1);
	
	return 0;
}
