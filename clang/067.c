#include <stdio.h>
#include <malloc.h>
#include <string.h>

void test_malloc() {
	char* ptr = NULL;
	int i = 0;

	printf("====test_malloc()====\n");
	ptr = (char*)malloc(10);
	for (i = 0; i < 10; i++) {
		printf("%x ", ptr[i]);
	}
}

void test_calloc() {
	char* ptr = NULL;
	int i = 0;

	printf("\n====test_calloc()====\n");
	ptr = (char*)calloc(2, 5);
	for (i = 0; i < 2 * 5; i++) {
		printf("%x ", ptr[i]);
	}
}

void test_realloc() {
	char* str1 = (char*)malloc(6);
	char* str2 = "6789";

	strcpy(str1, "12345");
	printf("\n====test_realloc()====\n");
	printf("str1 = %s, str2 = %s\n", str1, str2);
	printf("1. str1_addr = %p, str2_addr = %p\n", str1, str2);
	if (NULL == realloc(str1, 10)) { // 返回值为NULL，表示失败
		printf("error: realloc failure\n");
	}

	printf("2. str1_addr = %p, str2_addr = %p\n", str1, str2);

	strcat(str1, str2);
	printf("str1 = %s, str2 = %s\n", str1, str2);
}

int main() {
	test_malloc();
	test_calloc();
	test_realloc();
	return 0;
}