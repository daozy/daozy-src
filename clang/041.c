#include <stdio.h>
#include <stdlib.h>

int main() {
	unsigned m = 0;
	unsigned n = 0;
	unsigned** maze = NULL;
	unsigned i = 0;
	unsigned j = 0;

start:
	printf("Please input m,n: ");
	scanf("%u%u", &m, &n);  // m = 4, n = 5

	maze = (unsigned**)malloc(sizeof(unsigned*) * n);
	for (i = 0; i < n; i++) {
		maze[i] = (unsigned*)malloc(sizeof(unsigned) * m);
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) {
			maze[i][j] = rand() % 2;
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) {
			printf("%u ", maze[i][j]);
		}
		printf("\n");
	}

	for (i = 0; i < n; i++) {
		free(maze[i]);
		maze[i] = NULL;
	}
	free(maze);
	maze = NULL;

	goto start;
	
	return 0;
}