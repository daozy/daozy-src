#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void test_ldiv(const long a, const long b) {
	ldiv_t ret = {0};
	printf("====test_ldiv====\n");
	ret = ldiv(a, b);
	printf("quot=%ld, rem=%ld\n", ret.quot, ret.rem);
}

void test_log10(const double num) {
	double ret = 0.0;
	printf("====test_log10====\n");
	ret = log10(num);
	printf("num=%lf, ret = %lf\n", num, ret);
}

void test_modf(const double num) {
	double ret = 0.0;
	double i = 0;
	printf("====test_modf====\n");
	ret = modf(num, &i);
	printf("num = %lf, i = %lf, ret = %lf\n", num, i, ret);
}

void test_pow(const double base, const double exp) {
	printf("====test_pow====\n");
	printf("base = %lf, exp = %lf, ret = %lf\n", base, exp, pow(base, exp));
}

void test_sqrt(const double num) {
	printf("====test_sqrt====\n");
	printf("num = %lf, ret = %lf\n", num, sqrt(num));
}

int main() {
	test_ldiv(100, 3);
	test_log10(100);
	test_log10(-100);
	test_log10(0);
	test_modf(100.1);
	test_pow(10, 2);
	test_sqrt(100);
	test_sqrt(1000);
	test_sqrt(10000);
	return 0;
}