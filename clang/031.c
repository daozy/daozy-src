#include <stdio.h>

// 定义简单的结构体
// 0: 未上学，1：上学
typedef struct {
	char mon;
	char tue;
	char wed;
	char thur;
	char fir;
} StudAbsent1;


// 定义位域结构体
typedef struct {
	unsigned char mon:1;
	unsigned char tue:1;
	unsigned char wed:1;
	unsigned char thur:1;
	unsigned char fir:1;
} StudAbsent2;


void combination() {
	// 学生信息
	typedef struct {
		char name[32];
		unsigned age : 3;
		unsigned sex : 1;
		char class[32];
	} Student;
	
	
	Student stud1 = {.name = "zhangsan", .age = 6, .sex = 0, .class = "二班"};
	Student stud2 = {.name = "zhangsan", .age = 6, .sex = 0, .class = "二班"};
	
	printf("stud1.size = %lu\n", sizeof(Student));
	printf("name=%s, age=%u, sex=%u, class=%s\n",
			stud1.name, stud1.age, stud1.sex, stud1.class);
	printf("name=%s, age=%u, sex=%u, class=%s\n",
			stud2.name, stud2.age, stud2.sex, stud2.class);
		
}

int main() {
	StudAbsent1 absent1 = {0};
	StudAbsent2 absent2 = {0};
	
	absent1.mon = 1;
	absent1.tue = 1;
	absent1.wed = 0;
	absent1.thur = 1;
	absent1.fir = 1;

	printf("absent1.size = %lu\n", sizeof(StudAbsent1));
	
	absent2.mon = 1;
	absent2.tue = 1;
	absent2.wed = 0;
	absent2.thur = 1;
	absent2.fir = 1;

	printf("absent2.size = %lu\n", sizeof(StudAbsent2));

	combination();

	return 0;
}
