#include <stdio.h>
#include <string.h>

typedef struct {
	char name[32];
	char sex;
	unsigned age;
	char class[32];
} Student;

Student search(const Student stud[], const char* name);
void print_info(const Student stud[]);

int main() {
	// kind 1:
	Student stud1[10] = {};

	// kind 2:
	Student stud2[10] = {
		{"zhangsan", 'n', 12, "二班"},
		{.name = "lisi", .sex = 'n', .age = 13, .class= "三班"},
		{.sex = 'n', .age = 13, .class = "三班", .name = "lisi"},
		{name : "lisi", sex : 'n', age : 13, class: "三班"},
		{sex : 'n', age : 13, class: "三班", name : "lisi"},
		{"", '\0', 0, ""}
	};

	Student xstud = {};

	print_info(stud1);
	print_info(stud2);

	xstud = search(stud2, "zhangsan");
	printf("found : name=%s, sex=%c, age=%d, class=%s\n",
			xstud.name, xstud.sex, xstud.age, xstud.class);
	
	memset(&xstud, 0, sizeof(Student));
	xstud = search(stud2, "xlisi");
	printf("found : name=%s, sex=%c, age=%d, class=%s\n",
			xstud.name, xstud.sex, xstud.age, xstud.class);


	return 0;
}

Student search(const Student stud[], const char* name) {
	Student xstud = {};
	int i = 0;

	for (i = 0; i < 10; i++) {
		if (strcmp(stud[i].name, name) == 0) {
			xstud = stud[i];
			break;
		}
	}

	return xstud;
}

void print_info(const Student stud[]) {
	int i = 0;

	printf("----------------\n");
	for (i = 0; i < 10; i++) {
		if (strlen(stud[i].name) == 0) {
			break;
		}

		printf("%d. name=%s, sex=%c, age=%d, class=%s\n",
				i, stud[i].name, stud[i].sex, stud[i].age, stud[i].class);
	}
}
