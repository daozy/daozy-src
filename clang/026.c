#include <stdio.h>

void print_ret(const char* str, const int c) {
	printf("%s = %d\n", str, c);
}

int main() {
	int a = 0;
	int b = 0;
	int t = 0;

	printf("Please input a and b: ");
	scanf("%d%d", &a, &b);
	t = a;
	
	print_ret("a + b", a + b);
	print_ret("a += b", a += b);
	a = t;
	print_ret("a -= b", a -= b);
	a = t;
	print_ret("a *= b", a *= b);
	a = t;
	print_ret("a /= b", a /= b);
	a = t;
	print_ret("a %= b", a %= b);
	a = t;
	print_ret("a <<= b", a <<= b);
	a = t;
	print_ret("a >>= b", a >>= b);
	a = t;
	print_ret("a &= b", a &= b);
	a = t;
	print_ret("a |= b", a |= b);
	a = t;
	print_ret("a ^= b", a ^= b);

	return 0;
}
