void swap(int a, int b) {
	a=a^b;
	b=b^a;
	a=a^b;
}

int abs( int x ) { 
	int y=x>>31 ; 
	return(x^y)-y;//也可写作 (x+y)^y 
}

int CountNumberOfOne(int number)
{
	int counter = 0;
	while (number)
	{
		counter++;
		number &= number - 1 ;
	}
	return counter;
}
