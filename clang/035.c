/*************************************************************************
	> File Name: 035.c
	> Author: WangTeacher
	> Mail: daozy@163.com 
	> Created Time: 2020年06月07日 星期日 08时01分17秒
 ************************************************************************/

#include<stdio.h>

void read_only_ptr() {
	int a = 10;
	int b = 20;
	const int *p = &a;
	// const int *p = &a;
	// int const *p = &a;
	// int * const p = &a;
	// int * p const = &a;
	
	a = 20;
	*p = 20;
	

	p = &b;
	// *p = 30;
	printf("*p = %d\n", *p);
}

int main() {
	const int a = 0;
	const char c = 'A';

	// a = 10;
	// c = 'a';
	
	read_only_ptr();

	return 0;
}
