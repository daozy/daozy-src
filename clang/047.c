#include <stdio.h>

void char_to_int() {
	// char -> int
	char c = 'A';
	int d = 0;

	printf("c = %c, d = %d\n", c, d);
	d = c;
	printf("c = %c, d = %d\n", c, d);
	d = d + 1;
	c = d;
	printf("c = %c, d = %d\n", c, d);
	printf("c = %d, d = %d\n", c, d);
}

void int_to_float() {
	// int <-> float
	int a = 10;
	float f = 1.34F; // default double

	// 强制类型转换
	f = (float)a;
	printf("a = %d, f = %f\n", a, f);

	f = 1.99F;
	a = (int)f;
	printf("a = %d, f = %f\n", a, f);
}

void float_to_double() {
	float f = 1.234F;
	double ff = 2.345;

	f = (float)ff;
	printf("f = %f, ff = %lf\n", f, ff);

	f = 1.234F;
	ff = f;
	printf("f = %f, ff = %lf\n", f, ff);
}

void int_to_int() {
	short d1 = 1;
	int d2 = 2;
	long int d3 = 3;
	long long d4 = 40000000;
	unsigned d5 = 5;

	int t = 0;
	short ts = 0;

	// short -> int
	printf("len(short) = %d, len(int) = %d\n", sizeof(short), sizeof(int));
	t = d1;

	// int -> short
	ts = (short)d4;

	printf("ts = %d, d4 = %lld\n", ts, d4);
}


int main() {
	// char,int,float,double
	// case1: char <-> int
	//char_to_int();

	// case2: int <-> float
	//int_to_float();

	// case3: float <-> double
	// float_to_double();

	// case4: int <-> int (short int, unsinged int, long int, long long)
	int_to_int();

	return 0;
}