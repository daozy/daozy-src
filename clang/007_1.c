#include <stdio.h>

int main() {
	unsigned int score = 0;
	char grade = '\0';

	printf("please input your score: ");
	scanf("%d", &score);

	if (score >= 90) {
		grade = 'A';	
	} else if (score >= 60) {
		grade = 'B';
	} else {
		grade = 'C';
	}

	printf("%c\n", grade);

	return 0;
}
