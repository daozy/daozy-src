#include <stdio.h>

struct Book {
	int a;
	int b;
} book;

void show_sizeof() {
	int a = 0;
	int b[10] = {};
	char c[10] = {};

	printf("sizeof(a) = %lu\n", sizeof(a));
	printf("sizeof(b) = %lu\n", sizeof(b));
	printf("sizeof(c) = %lu\n", sizeof(c));
	printf("sizeof(book) = %lu\n", sizeof(book));
}

void show_point() {
	int a = 2;
	int *p = &a;

	printf("p = %p\n", p);
	printf("*p = %d\n", *p);
}

void show_three() {
	int a = 0;
	int b = 1;	
	int c = 0;

	/**
	if (a != 0) {
		c = a;
	} else {
		c = b;
	}
	*/
	
	c = a != 0 ? a + b : a - b;

	printf("c = %d\n", c);
}

int main() {
	// show_sizeof();
	// show_point();
	show_three();	
 
	return 0;
}









