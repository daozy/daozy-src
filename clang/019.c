#include <stdio.h>

int min(const int m, const int n);

int max(const int m, const int n) {
	if (m > n) {
		return m;
	}

	return n;
}

int sum(const int m, const int n) {
	return m + n;
}

int diff(const int m, const int n) {
	return m - n;
}

int product(const int m, const int n) {
	return m * n;
}

int main() {
	int m = 0;
	int n = 0;

	scanf("%d", &m);
	scanf("%d", &n);

	printf("min: %d\n", min(m, n));
	printf("max: %d\n", max(m, n));
	printf("sum: %d\n", sum(m, n));
	printf("diff: %d\n", diff(m, n));
	printf("product: %d\n", product(m, n));

	return 0;
}

int min(const int m, const int n) {
	if (m > n) {
		return n;	
	}
	return m;
}
