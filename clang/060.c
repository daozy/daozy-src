#include <stdio.h>
#include <time.h>

void test_time() {
	// typedef __int64 __time64_t;
	// typedef __time64_t time_t;
	time_t ret = 0;
	printf("====test_time====\n");
	// ���ص���
	printf("time = %lld\n", time(NULL));
	time(&ret);
	printf("ret = %lld\n", ret);
}

void test_clock() {
	// typedef long clock_t;
	int i = 0;
	printf("====test_clock====\n");
	// ���ص�΢��
	printf("start = %ld\n", clock());
	while (i < 10000) { i++;  }
	printf("end = %ld\n", clock());
}

void test_ctime() {
	time_t t = 0;
	char* str = NULL;
	time(&t);
	printf("====test_ctime====\n");
	str = ctime((const time_t*)&t);
	printf("time_str: %s\n", str);
}

int main() {
	test_time();
	test_clock();
	test_ctime();
	return 0;
}