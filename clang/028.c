#include <stdio.h>

struct Student {
	char name[32];
	char sex;
	unsigned age;
	char class[32];
};

int main() {
	struct Student stud;
	
	printf("name: ");
	scanf("%s", stud.name);
	printf("sex: ");
	scanf("\n%c", &stud.sex);
	printf("age: ");
	scanf("%d", &stud.age);
	printf("class: ");
	scanf("%s", stud.class);

	printf("name: %s\n", stud.name);
	printf("sex: %c\n", stud.sex);
	printf("age: %u\n", stud.age);
	printf("class: %s\n", stud.class);

	return 0;
}
