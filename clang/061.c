#include <stdio.h>
#include <time.h>

/*
    struct tm
    {
        int tm_sec;   // seconds after the minute - [0, 60] including leap second
        int tm_min;   // minutes after the hour - [0, 59]
        int tm_hour;  // hours since midnight - [0, 23]
        int tm_mday;  // day of the month - [1, 31]
        int tm_mon;   // months since January - [0, 11]
        int tm_year;  // years since 1900
        int tm_wday;  // days since Sunday - [0, 6]
        int tm_yday;  // days since January 1 - [0, 365]
        int tm_isdst; // daylight savings time flag
    };
*/

void test_localtime() {
	struct tm *t = { 0 };
    time_t it = 0;
    time(&it);
	printf("====test_localtime====\n");
	t = localtime((const time_t*)&it);
	printf("%d-%d-%d %d:%d:%d\n", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
}

void test_difftime() {
    time_t t1 = 0;
    time_t t2 = 0;
    int i = 0;
    printf("====test_difftime====\n");

    time(&t1);
    while (i < 1000000000) { i++; }
    time(&t2);
    
    printf("t1 = %lld, t2 = %lld, diff = %lf\n", t1, t2, difftime(t1, t2));
}

void test_asctime() {
    struct tm* ptr = NULL;
    time_t it = 0;
    printf("====test_difftime====\n");
    
    time(&it);
    ptr = localtime((const time_t*)&it);
    printf("time_str: %s\n", asctime(ptr));
}


int main() {
	test_localtime();
    test_difftime();
    test_asctime();
	return 0;
}