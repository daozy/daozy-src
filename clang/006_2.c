#include <stdio.h>
#include <float.h>

int main(int argc, char* argv[]) {
    printf("float 存储最大字节数 : %lu \n", sizeof(float));
    printf("float 最小值: %E\n", FLT_MIN );
    printf("float 最大值: %E\n", FLT_MAX );
    printf("float 精度值: %u\n", FLT_DIG );

    printf("double 存储最大字节数 : %lu \n", sizeof(double));
    printf("double 最小值: %E\n", DBL_MIN );
    printf("double 最大值: %E\n", DBL_MAX );
    printf("double 精度值: %u\n", DBL_DIG );

    printf("long double 存储最大字节数 : %lu \n", sizeof(long double));
    printf("long double 最小值: %LE\n", LDBL_MIN );
    printf("long double 最大值: %LE\n", LDBL_MAX );
    printf("long double 精度值: %u\n", LDBL_DIG );    

    return 0;
}