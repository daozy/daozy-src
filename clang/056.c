#include <stdio.h>
#include <string.h>

void test_memchr() {
	char* ret = NULL;
	printf("====test_memchr====\n");
	ret = memchr("abcdef", 'd', 3);
	if (ret) {
		printf("ret = %s\n", ret);
	}
	else {
		printf("Don't found this char\n");
	}
}

void test_memcmp() {
	int ret = 0;
	printf("====test_memchr====\n");
	ret = memcmp("abcd22233", "abcd222", 7);
	printf("%s\n", ret > 0 ? "buf1 > buf2" : ret < 0 ? "buf1 < buf2" : "buf1 == buf2");
}

void test_memcpy() {
	char buf1[100] = "11111111";
	char buf2[100] = "123456789";
	char* ret = NULL;
	printf("====test_memcpy====\n");
	ret = memcpy(buf1, buf2, 9);
	printf("%s\n", ret);
}

void test_memmove() {
	char buf[11] = "0123456789";
	char* buf1 = buf;
	char* buf2 = buf + 4; // ָ��buf��'4'
	char* ret = NULL;
	printf("====test_memmove====\n");
	printf("buf1 = %s, buf2 = %s\n", buf1, buf2);
	ret = memmove(buf1, buf2, 6);
	printf("ret = %s, buf2 = %s\n", ret, buf2);
}

void test_memset() {
	char buf[10] = "123456789";
	printf("====test_memset====\n");
	printf("buf = %s, sizeof(buf) = %d, strlen(buf) = %d\n", buf, sizeof(buf), strlen(buf));
	memset(buf, 'a', sizeof(buf) - 1);
	printf("buf = %s\n", buf);
}

int main() {
	test_memchr();
	test_memcmp();
	test_memcpy();
	test_memmove();
	test_memset();

	getchar();

	return 0;
}