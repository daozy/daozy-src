#include <stdio.h>

// 程序块要采用缩进风格编写，缩进的空格数为4.
void rule1(int x) {
	int i = -1;

	if (x > 0) {
		i = 1;
	}
	else {
		i = 0;
	}
}

//相对独立的程序块之间、变量说明之后必须加空行。
void rule2(int x) {
	int a = 0;
	int b = 0;
	int sum = 0;

	// 求和
	sum = a + b;

	// 求乘积
	sum = a * b;
}

//较长的语句（ > 80字符）要分成多行书写.操作符放在新行之首
void rule3() {
	int a[] = { 1,2,3,4,5,6 };
	if (a[0] > 0 && a[1] > 0 && a[2] > 0 
		&& a[3] > 0 && a[4] > 0 && a[5] > 0) {
		return;
	}
}

//不允许把多个短语句写在一行中，即一行只写一条语句。
void rule4() {
	int a = 0; int b = 0; // 错误
	int x = 0;
	int y = 0;
}

//if、for、do、while、case、switch、default等语句自占一行，且if、for、 do、while等语句的执行语句部分无论多少都要加括号{}。
int rule5(int x) {
	if (x > 0) return 100; //  错误示范

	// 正确示范
	if (x > 0) {
		return 100;
	}

	return 0;
}

//函数或过程的开始、结构的定义及循环、判断等语句中的代码都要采用缩进风格，case 语句下的情况处理语句也要遵从语句缩进要求。
void rule6(int x) {
	// 错误示范
	int a = 0;

	// 正确示范
	int b = 0;

	// 错误示范
	struct Node {
		int v;
	};

	// 正确
	struct Node1 {
		int v;
	};

	// 错误
	switch (x) {
	case 0: return 0; break;
	case 1: return 1; break;
	default: return -1;
	}

	// 正确
	switch (x) {
		case 0: 
			a = 0;
			break;
		case 1:
			a = 1;
			break;
		default:
			a = -1;
			break;
	}
}
//在两个以上的关键字、变量、常量进行对等操作时，它们之间的操作符之前、之后或 者前后要加空格；进行非对等操作时，如果是关系密切的立即操作符（如－ > ），后不应加空
void rule7() {
	int a = 0;
	
	// 错误
	a=a+1;
	// 正确
	a = a + 1;

	// 错误
	a ++;
	++ a;
	// 正确
	a++;
	++a;

	typedef struct _Node {
		int v;
	} Node;
	Node* ptr = NULL;
	//错误
	ptr -> v = 1;
	//正确
	ptr->v = 1;
}

//函数头部应进行注释，列出：函数的目的 / 功能、输入参数、输出参数、返回值、调用 关系（函数、表）等。

/**
 * 功能：求两数之和
 * 参数1：a, 第一个数
 * 参数2：b, 第二个数
 * 返回值：a和b之和
 */
int add(int a, int b) {
	return a + b;
}

int main() {
	return 0;
}