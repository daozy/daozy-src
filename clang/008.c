#include <stdio.h>

void fun1(const int x) {
	int true = 1;
	int false = 0;
	int ret = 0;
	
	if (x == 0) {
		ret = false;
	} else {
		ret = true;
	}

	if (ret) {
		printf("fun1: true\n");
	} else {
		printf("fun1: false\n");
	}
}

void fun2(const int x) {
	typedef enum {false, true} bool;
	bool ret = false;

	if (x == 0) {
		ret = false;
	} else {
		ret = true;
	}

	if (ret) {
		printf("fun2: true\n");
	} else {
		printf("fun2: false\n");
	}
}

void fun3(const int x) {
#define false 0
#define true 1

	int ret = false;

	if (x == 0) {
		ret = false;
	} else {
		ret = true;
	}

	if (ret) {
		printf("fun3: true\n");
	} else {
		printf("fun3: false\n");
	}
}

int main() {
	int x = 0;
	
	printf("please input a int: ");
	scanf("%d", &x);

	fun1(x);
	fun2(x);
	fun3(x);

	return 0;
}
