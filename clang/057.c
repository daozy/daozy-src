#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void test_abs(const int num) {
	printf("====test_abs====\n");
	printf("abs(%d) = %d\n", num, abs(num));
}

void test_labs(const long num) {
	printf("====test_labs====\n");
	printf("labs(%ld) = %ld\n", num, labs(num));
}

void test_llabs(const long long num) {
	printf("====test_llabs====\n");
	printf("llabs(%lld) = %lld\n", num, llabs(num));
}

void test_fabs(const double num) {
	printf("====test_fabs====\n");
	printf("fabs(%lf) = %lf\n", num, fabs(num));
}

void test_ceil(const double num) {
	printf("====test_ceil====\n");
	printf("ceil(%lf) = %lf\n", num, ceil(num));
}

void test_div(const int numerator, const int denominator) {
	div_t ret = {0};
	printf("====test_div====\n");
	ret = div(numerator, denominator);
	printf("%d/%d = %d/%d\n", numerator, denominator, ret.quot, ret.rem);
}

int main() {
	printf("sizoef(int) = %d, sizeof(long int) = %d, sizeof(long long) = %d\n", 
		sizeof(int), sizeof(long int), sizeof(long long));
	test_abs(10);
	test_abs(-10);

	test_labs(1000000);
	test_labs(-1000000);

	test_llabs(100000000000);
	test_llabs(-100000000000);

	test_fabs(12321.2342);
	test_fabs(-12321.2342);

	test_ceil(1.234);
	test_ceil(2.50);
	test_ceil(3.49);

	test_div(10, 3);
	test_div(10, 5);

	getchar();
	return 0;
}