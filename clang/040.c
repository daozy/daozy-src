#include <stdio.h>

typedef int ID;
typedef int (*Min)(int, int);
typedef int (*Max)(int, int);
typedef int (*Sum)(int, int);

int min(int x, int y) {
	return x < y ? x : y;
}

int max(int x, int y, Sum sum) {
	int t = x > y ? x : y;
	return t > sum(x, y) ? t : sum(x, y);
}

int sum(int x, int y) {
	return x + y;
}

void fun(Max p_max, Min p_min, Sum p_sum) {
	printf("max = %d\n", p_max(1, 2));
	printf("min = %d\n", p_min(1, 2));
	printf("sum = %d\n", p_sum(1, 2));
}

int main() {
	//Sum p_sum = &sum;
	//printf("max = %d\n", max(1, 2, p_sum));
	fun(max, min, sum);

	return 0;
}

