#include <stdio.h>

void sort(int x[], const int len) {
	int i = 0;
	int j = 0;
	int temp = 0;

	for (i = 0; i < len - 1; i++) {
		for (j = 0; j < len - i - 1; j++) {
			if (x[j] > x[j + 1]) {
				temp = x[j];
				x[j] = x[j + 1];
				x[j + 1] = temp;
			}
		}
	}
}

int main() {
	int x[] = { 34, 42, 23, 78, 12, 65, 98 };
	int len = sizeof(x) / sizeof(int);
	int i = 0;

	sort(x, len);

	for (i = 0; i < len; i++) {
		printf("%d ", x[i]);
	}

	printf("\n");
	return 0;
}