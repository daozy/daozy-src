#include <stdio.h>

int main() {
	char ch = '\'';
	char dch = '"';
	char xch = '?';

	printf("\\t: \t.\n");
	printf("\\': %c.\n", ch);
	printf("\\\?: %c.\n", xch);
	printf("\\a: \a.\n");

	return 0;
}
