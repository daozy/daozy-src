#include <stdio.h>
#include "def.h"

extern int x;

int a = 0;
int b = 0;

void test() {
	int a = 20;
	printf("test(), a = %d\n", a);
}

int main() {
	int a = 10;

	printf("main(), a = %d\n", a);
	test();

	printf("main(), b = %d\n", b);

	printf("x = %d\n", x);

	return 0;
}