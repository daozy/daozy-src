#include <stdio.h>
#include <string.h>


int gen_file(const char *path) {
	const char* content = "www.daozy.net";
	const unsigned cnt = 100;
	unsigned i = 0;
	FILE* fp = NULL;

	fp = fopen(path, "w");
	if (NULL == fp) {
		return -1;
	}

	for (i = 0; i < cnt; i++) {
		fputs(content, fp);
	}

	printf("write len = %u\n", strlen(content) * cnt);
	fclose(fp);

	return 0;
}

int file_len(const char *path) {
	FILE* fp = fopen(path, "r");
	if (NULL == fp) {
		return -1;
	}

	printf("0. ftell = %u\n", ftell(fp));

	// test 1
	fseek(fp, 10, SEEK_SET);
	printf("1. ftell = %u\n", ftell(fp));

	// test 2
	fseek(fp, 10, SEEK_CUR);
	printf("2. ftell = %u\n", ftell(fp));

	// test 3
	fseek(fp, 10, SEEK_END);
	printf("3. ftell = %u\n", ftell(fp));

	fseek(fp, 0, SEEK_END);
	printf("file length = %u\n", ftell(fp));

	fclose(fp);

	return 0;
}

void get_get_pos(const char* path) {
	FILE* fp = NULL;
	fpos_t position = 0;
	char buf[1024] = { 0 };

	fp = fopen(path, "r");
	if (NULL == fp) {
		return;
	}

	printf("ftell = %u\n", ftell(fp));
	fgets(buf, 32, fp);
	fgetpos(fp, &position);
	printf("1. fgetpos = %u\n", (unsigned)position);

	position = 64;
	fsetpos(fp, &position);
	fgets(buf, 32, fp);
	fgetpos(fp, &position);
	printf("2. fgetpos = %u\n", (unsigned)position);

	fclose(fp);
}

int main() {
	const char* path = "36.txt";
	// gen_file(path);
	// file_len(path);
	// fgetpos, fsetpos
	get_get_pos(path);

	return 0;
}