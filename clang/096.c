#include <stdio.h>

int main() {
	int a = 10; 
	int b = 20;

	printf("a = %u, b = %u\n", a, b);
	a = a + b;
	b = a - b;
	a = a - b;
	printf("a = %u, b = %u\n", a, b);

	return 0;
}