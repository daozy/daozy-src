#include <stdio.h>
#include <math.h>

void test_floor(const double arg) {
	printf("====test_floor====\n");
	printf("floor(%lf) = %lf\n", arg, floor(arg));
}

void test_fmod(const double x, const double y) {
	printf("====test_fmod====\n");
	printf("fmod(%lf, %lf) = %lf\n", x, y, fmod(x, y));
}

void test_frexp(const double num) {
	double ret = 0.0;
	int exp = 0;
	printf("====test_frexp====\n");
	ret = frexp(num, &exp);
	printf("%lf = %lf * 2^%d \n", num, ret, exp);
}

void test_ldexp(const double num, const int exp) {
	double ret = 0.0;
	printf("====test_frexp====\n");
	// num * (2 ^  exp)
	ret = ldexp(num, exp);
	if (ret == HUGE_VAL) {
		printf("error: ret = %lf\n", HUGE_VAL);
	}
	else {
		printf("%lf * 2^%d = %lf\n", num, exp, ret);
	}
}

int main() {
	test_floor(1.234);
	test_floor(100.234);
	test_floor(1000.234);

	test_fmod(10, 3);
	test_fmod(100, 30);
	test_fmod(10.123, 3.456);

	test_frexp(100);
	test_frexp(1024);

	test_ldexp(1, 10);
	test_ldexp(2, 9);
	test_ldexp(1, 9000);

	return 0;
}