#include <stdio.h>

extern int add(const int a, const int b);

void test_static() {
	int a = 10;
	static int b = 20;

	a++;
	b++;

	printf("a = %d, b = %d\n", a, b);
}

int main() {
	int a[] = { 1, 2, 3, 4, 5 };

	test_static();
	test_static();

	printf("add = %d\n", add(1, 2));

	return 0;
}