
static int _add(const int a, const int b) {
	return a + b;
}

int add(const int a, const int b) {
	return _add(a, b);
}