/*
 * @file hello.c
 * 接受输入，然后和hello world一起输出
 * @author WangTeacher
 */

// 预处理器指令
// 引入头文件
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

/*
 * @brief：主函数
 * @argc: 命令行参数个数 
 * @argv: 参数值列表
 */
int main(int argc, char* argv[]) { /* 语句块开始 */
    // 变量
    char str[100] = {0};

    // 语句
    // gets(str);
    scanf("%[^\n]%*c", str);

    // 调用函数
    printf("Hello, World!\n");
    printf("%s\n", str);

    // 返回值
    return 0;
} /* 语句块结束 */

// 编译
// $ gcc hello.c
// 运行
// $ a.out


























// #include <stdio.h>
// #include <string.h>
// #include <math.h>
// #include <stdlib.h>

// int main() {
//     char ch = 0;
//     char str1[100] = {0};
//     char str2[100] = {0};

//     scanf("%c", &ch);
//     scanf("%s", str1);
//     scanf("\n");
//     scanf("%[^\n]%*c", str2);

//     printf("%c\n", ch);
//     printf("%s\n",  str1);
//     printf("%s",  str2);

//     return 0;
// }
