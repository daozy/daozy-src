#include <stdio.h>
#include <stdlib.h>

typedef struct _Node {
	int v;
	struct _Node* next;
} Node;

void print(Node* header) {
	Node* ptr = header;
	
	while (ptr != NULL) {
		printf("ptr->v = %d\n", ptr->v);
		ptr = ptr->next;
	}

	printf("\n");
}

void reverse(Node** header) {
	Node* ptr = *header;
	Node* q = NULL; // 下一个
	Node* p = NULL; // 上一个

	while (ptr) {
		q = ptr->next;
		ptr->next = p;
		p = ptr;
		ptr = q;
	}

	*header = p;
}

int main() {
	Node* header = (Node*)malloc(sizeof(Node));
	Node* ptr = header;
	ptr->v = 1;
	ptr->next = (Node*)malloc(sizeof(Node));
	
	ptr = ptr->next;
	ptr->v = 2;
	ptr->next = (Node*)malloc(sizeof(Node));

	ptr = ptr->next;
	ptr->v = 3;
	ptr->next = (Node*)malloc(sizeof(Node));

	ptr = ptr->next;
	ptr->v = 4;
	ptr->next = NULL;

	print(header);
	
	reverse(&header);
	
	printf("---------------------\n");
	print(header);

	return 0;
}