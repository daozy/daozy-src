#include <stdio.h>

#ifdef __cplusplus
#error this file should be processed with C++ Compiler
#endif

#pragma message("1111111")

#define MY
#define XX

// #undef, #if, #else, #elif, #endif
int main() {
	int age = 0; // 这个变量在使用中不可能为零，如果为零让编译器报错

#ifdef MY
	printf("define MY\n");
#else
	printf("not define MY\n");
#endif

#undef MY  // 去掉定义，使之没有定义

#ifdef MY
	printf("define MY\n");
#else
	printf("not define MY\n");
#endif


#if (3 - 3)
	printf("if 1\n");
#elif 3 - 3 == 0
	printf("if 0\n");
#endif

#pragma message("2222222222")

	return 0;
}