#include <stdio.h>
#include <stdlib.h>

void test_atof() {
	printf("====test_atof====\n");
	printf("ret = %lf\n", atof("22.222233333e"));
}

void test_atoi() {
	int ret = 123456789111;
	printf("====test_atoi====, sizeof(int) = %d\n", sizeof(int));
	ret = atoi("123456789111");
	printf("ret = %d\n", ret);
}

void test_atol() {
	long long int ret = 0;
	printf("====test_atol====, sizeof(long long) = %d\n", sizeof(long long));
	ret = atoll("123456789111");
	printf("ret = %lld\n", ret);
}

void test_is() {
	printf("====test_is====\n");
	printf("a is %s\n", isalnum('a') != 0 ? "YES" : "NO");
	printf("? is %s\n", isalnum('?') != 0 ? "YES" : "NO");
	printf("8 is %s\n", isalnum('8') != 0 ? "YES" : "NO");

	printf("? is %s\n", isalpha('?') != 0 ? "YES" : "NO");
	printf("a is %s\n", isalpha('a') != 0 ? "YES" : "NO");

	printf("a is lower %s\n", islower('a') != 0 ? "YES" : "NO");
	printf("A is lower %s\n", islower('A') != 0 ? "YES" : "NO");

	printf("A to %c\n", tolower('A'));
}

int main() {
	test_atof();
	test_atoi();
	test_atol();
	test_is();
	getchar();
	return 0;
}