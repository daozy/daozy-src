#include <stdio.h>

void switch_select(const int kind) {
	switch (kind) {
		case 1:
		case 2:
			if (kind == 1) {
				printf("《人工智能》\n");
			} 
			else if (kind == 2) {
				printf("《老人与海》\n");
			}

			printf("《时间简史》\n");
			break;
		case 3:
			printf("《明朝的那些事儿》\n");
			break;
		default:
			printf("什么都没有找到\n");
			break;
	}
}

void if_select(const int kind) {
	if (kind == 1) {
		printf("a.《人工智能》\n");
	} else if (kind == 2) {
		printf("b.《老人与海》\n");
	} else if (kind == 3) {
		printf("c.《明朝的那些事儿》\n");
	} else {
		printf("什么都没有找到\n");
	}
}

int main() {
	int kind = 0;
	char seq = 0;

	printf("Book Query System\n");
	printf("1. 科技，2. 文学，3. 历史\n");
	printf("Please select number(1~3):");
	scanf("%d", &kind);

	//switch_select(kind);
	if_select(kind);
	printf("Please input the book seq:");
	scanf("\n%c", &seq);

	switch (seq) {
		case 'a':
			printf("a.《人工智能》\n");
			break;
		case 'b':
			printf("b.《老人与海》\n");
			break;
		case 'c':
			printf("c.《明朝的那些事儿》\n");
			break;
		default:
			printf("Don't found this book.\n");
			break;
	}
	
	return 0;
}
