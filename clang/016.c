#include <stdio.h>

#define MAX_SIZE 128

int main() {
	int inputs[MAX_SIZE] = {0};
	int n = 0;
	int i = 0;
	int tmp = 0;
	int max = 0;
	
	printf("Please input N: ");
	scanf("%d", &n);
	if (n > MAX_SIZE) {
		printf("N must < %d\n", MAX_SIZE);
		return -1;
	}

	printf("Please input N int: ");
	while (i < n && scanf("%d", &tmp)) {
		inputs[i] = tmp;
		i++;
	}

	// for 
	//for (i = 0; i < n; i++) {
	//	if (inputs[i] > max) {
	//		max = inputs[i];
	//	}
	//}
	
	//i = 0;
	//do {
	//	if (inputs[i] > max) {
	//		max = inputs[i];
	//	}

	//	i++;
	//} while (i < n);
	
	i = 0;
start:
	if (inputs[i] > max) {
		max = inputs[i];
	}

	i++;

	if (i >= n) {
		goto end;
	}

	goto start;

end:
	
	printf("max: %d\n", max);

	return 0;
}
