#include <stdio.h>

int main() {
	int a = 100;
	int b = 200;
	const int* p1 = &a;
	int const* p2 = &a;
	int* const p3 = &a;


	// 1. const int* p1
	p1 = &b; // 不是修饰的指针本身
	//*p1 = 300; // 修饰的是指针指向的变量，所以变量的地址不允许修改

	// 2. int const* p2, 等同于1
	p2 = &b;
	//*p2 = 300;

	// 3. int* const p3 = &a;
	// p3 = &b; // 修饰的是指针本身，不许修改
	*p3 = 300; // 指向的变量允许修改
}