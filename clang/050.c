#include <stdio.h>

#define M_SIZE 10000
#define P_SUM

int main() {
	int input[M_SIZE] = { 0 };
	int sum = 0;
	int i = 0;
	int k = 0;
	int n = 0;

	printf("Please input N int:\n");
	while ((n = scanf("%d", &input[i]))) {
		printf("n = %d, %d\n", n, input[i++]);
	}

	printf("n = %d, %d, len = %d\n", n, input[i], i);

	for (k = 0; k < i; k++) {
		sum += input[k];
	}

#ifdef P_SUM
	printf("sum = %d\n", sum);
#endif

#ifndef P_SUM
	printf("sum = %d\n", sum);
#endif

	return 0;
}