#include <stdio.h>
#include <string.h>

//边写代码边注释，修改代码同时修改相应的注释，以保证注释与代码的一致性。不再有用的注释要删除。
int rule1() {
	// 相加的第一个数
	int a = 0;
	// 相加的第二个数
	int b = 0;
	// 两数之和
	int sum = 0;

	// 求两数之和
	sum = a + b;

	// 返回和
	return sum;
}

//对于switch语句下的case语句，如果因为特殊情况需要处理完一个case后进入下一 个case处理，必须在该case语句处理完、下一个case语句前加上明确的注释。
int rule2(int x) {
	int ret = 0;

	switch (x) {
		case 0:
			ret = 100;
			printf("ret = %d\n", ret);
			//break; // 因为需要下一个case判断
		case 1:
			ret = 200;
			printf("ret = %d\n", ret);
			break;
		case 2:
			ret = 300;
			break;
		default:
			break;
	}

	return ret;
}

//注意运算符的优先级，并用括号明确表达式的操作顺序，避免使用默认优先级。
void rule3() {
	int a = 0;
	int b = 0;
	int x = 0;
	int* ptr = &a;
		
	// 错误
	x = ++ptr + 1 + b++;
	// 正确
	x = (++ptr) + 1 + b++;
	x = ++(ptr + 1) + b++;
}

#define MAX_SIZE 1024
#define MIN_SIZE 128
//避免使用不易理解的数字，用有意义的标识来替代。
void rule4() {
	char buffer1[MAX_SIZE] = { 0 };
	char buffer2[MIN_SIZE] = { 0 };

	memcpy(buffer2, buffer1, MAX_SIZE);
}

//不要使用难懂的技巧性很高的语句，除非很有必要时。
void rule5() {
	int a[] = { 12,23,4,5 };
	int x = *((int*)a + 5); // a[4]
}

//去掉没必要的公共变量。
//int g_x = 0;
void rule6() {
	int g_x = 0;
	int x = g_x;
}

//防止局部变量与公共变量同名
int aa = 10;
void rule7() {
	int aa = 20;
	
	printf("%d\n", aa);
}

void search() {
	// 
}

void remove() {
	// 
}

//结构的功能要单一，是针对一种事务的抽象。
void rule8(const int op) {
	// 查找
	if (op == 1) {
		// search
		search();
	} else if (op == 2) {
		// 删除
		// remove
		remove();
	}
}

//函数的规模尽量限制在 200 行以内。一个函数仅完成一件功能。

int main() {
	return 0;
}