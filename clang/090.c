#include <stdio.h>
#include <string.h>

// 将 str 中的小写字母转换成大写字母
void upper_case(char str[], const unsigned long long len) { // C编译器干的事情，把数组默认转换成了指针。
	unsigned i = 0;
	printf("str字符长度为: %llu\n", len);
	for (i = 0; i < len; ++i) {
		if ('a' <= str[i] && str[i] <= 'z') {
			str[i] -= ('a' - 'A');
		}
	}
}

int main() {
	char str[] = "aBcDefffffffff";
	// strlen(str);
	// printf("str字符长度为: %u\n", sizeof(str)/sizeof(str[0]));
	printf("str字符长度为: %llu\n", sizeof(str) - 1);
	upper_case(str, strlen(str));
	printf("str = %s\n", str);
	return 0;
}