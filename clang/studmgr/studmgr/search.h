/**
 * @file: search.h
 * 查询功能
 */
#pragma once

/*
 * 查询功能入口
 */
void sm_search();

/*
 * 导出功能入口
 */
void sm_export();