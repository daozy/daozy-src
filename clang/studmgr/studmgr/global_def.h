/**
 * @file: global_def.h
 * 系统的全局类型定义
 */

#pragma once

#include <stdio.h>
#include <string.h>
#include <malloc.h>

// 最长用户名大小
#define MAX_USER_SIZE 64

// 最长密码大小
#define MAX_PWD_SIZE 32

// 最长文件路径
#define MAX_PATH 128

// 登录状态
typedef enum { LOGOUT, LOGIN } LoginStatus;

// 菜单功能
typedef enum { M_SEARCH = 1, M_IMPORT, M_MODIFY, M_DEL, M_EXPORT, M_EXIT } Menu;

// 字符串最大buffer
#define BUF_SIZE 32

// 最大课程数
#define MAX_COURSE 100

// 最大学员数
#define MAX_STUDS 1000

// 课程信息
typedef struct {
    char name[BUF_SIZE]; // 课程名 
    unsigned score; // 课程分数
} Course;

// 学员信息
typedef struct {
    char name[BUF_SIZE]; // 姓名
    char sex[BUF_SIZE]; // 性别
    unsigned age; // 年龄
    char id[BUF_SIZE]; // 学号
    char class[BUF_SIZE]; // 班级
    Course course[MAX_COURSE]; // 课程
} Student;

// 系统存储文件
#define STORE_FILE "studmgr.dat"