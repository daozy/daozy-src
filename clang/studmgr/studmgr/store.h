/**
 * @file: store.h
 * 存储模块
 */
#pragma once

#include "global_def.h"

/**
 * 导入单个学员信息
 */
int import_one(const Student* stud);

/**
 * 通过文件导入学员信息
 */
int import_file(const char* path);

/**
 * 导出到文件
 */
int export_file(const char* path, const Student studs[], const unsigned num);

/**
 * 查询：根据姓名、学号、班级、年龄
 * cond: 查询输入条件
 * results: 查询返回记录
 * num: results数组的最多存储大小
 * 返回值：查询结果集合的大小
 */
int find_by_keys(const Student* cond, Student results[], const unsigned num);

/**
 * 查询：列出所有
 * results: 返回的所有记录
 * num: results数组的大小
 * 返回值：查询结果的实际大小
 */
int find_list(Student results[], const unsigned num);

/**
 * 删除：根据姓名
 */
int del_by_keys(const Student* cond, Student results[], const unsigned num);

/**
 * 修改：先查询出来再修改
 */
int alert_from_find();
