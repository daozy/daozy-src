/**
 * @file: search.c
 * 查询功能
 */
#include "search.h"
#include "global_def.h"
#include "store.h"

static void _print(const Student studs[], const unsigned num) {
    unsigned i = 0;
    
    printf("---------------------------------------------------\n");
    printf("序号 | 姓名  |  性别  |  年龄  |  学号  |  班级 \n");
    for (i = 0; i < num; i++) {
        printf(" %u   | %-6s| %-6s | %-6u | %-6s | %-6s\n", i + 1, studs[i].name, studs[i].sex, studs[i].age, studs[i].id, studs[i].class);
    }
    printf("---------------------------------------------------\n");
}

static _search_by_cond() {
    Student stud;
    Student* results = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    unsigned count = 0;

    printf("====请输入查询条件====\n");
    printf("注：如果不需要查询条件，字符串类型输入*\n");
    printf("注：整数类型不需要的，输入0\n");
    printf(" 姓名: ");
    scanf("%s", stud.name);
    printf(" 性别: ");
    scanf("%s", &stud.sex);
    printf(" 年龄: ");
    scanf("%u", &stud.age);
    printf(" 学号: ");
    scanf("%s", stud.id);
    printf(" 班级: ");
    scanf("%s", stud.class);

    count = find_by_keys(&stud, results, MAX_STUDS);
    printf("查询到 %u 个结果.\n", count);
    if (count > 0) {
        _print(results, count);
    }

    free(results);
}

static void _list_all() {
    Student* results = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    unsigned count = 0;

    count = find_list(results, MAX_STUDS);
    printf("查询到 %u 个结果.\n", count);
    if (count > 0) {
        _print(results, count);
    }

    free(results);
}

static void _export_by_cond() {
    char path[MAX_PATH] = { 0 };
    Student stud;
    Student* results = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    unsigned count = 0;

    printf("====请输入查询条件====\n");
    printf("注：如果不需要查询条件，字符串类型输入*\n");
    printf("注：整数类型不需要的，输入0\n");
    printf(" 姓名: ");
    scanf("%s", stud.name);
    printf(" 性别: ");
    scanf("%s", &stud.sex);
    printf(" 年龄: ");
    scanf("%u", &stud.age);
    printf(" 学号: ");
    scanf("%s", stud.id);
    printf(" 班级: ");
    scanf("%s", stud.class);

    count = find_by_keys(&stud, results, MAX_STUDS);
    printf("查询到 %u 个结果\n", count);
    if (count <= 0) {
        free(results);
        return;
    }

    printf("请输入导出文件: ");
    scanf("%s", path);
    export_file(path, results, count);
    printf("导出到 %s 成功!!!!\n", path);

    free(results);
}

static void _export_all() {
    char path[MAX_PATH] = { 0 };
    Student* results = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    unsigned count = 0;

    count = find_list(results, MAX_STUDS);
    printf("查询到 %u 个结果\n", count);
    printf("请输入导出文件: ");
    scanf("%s", path);
    export_file(path, results, count);
    printf("导出到 %s 成功!!!!\n", path);

    free(results);
}

void sm_search() {
    unsigned select = 0;

    printf("====请选择查询方式====\n");
    printf(" 1. 根据各种条件查询.\n");
    printf(" 2. 列出所有.\n");
    printf(" 请选择序号：");
    scanf("%u", &select);
    
    if (select == 1) {
        _search_by_cond();
    }
    else if (select == 2) {
        _list_all();
    }
    else {
        printf("[error] 请输入正确的序号.\n");
    }
}

void sm_export() {
    unsigned select = 0;

    printf("====请选择导出方式====\n");
    printf(" 1. 根据各种条件导出.\n");
    printf(" 2. 导出所有.\n");
    printf(" 请选择序号：");
    scanf("%u", &select);

    if (select == 1) {
        _export_by_cond();
    }
    else if (select == 2) {
        _export_all();
    }
    else {
        printf("[error] 请输入正确的序号.\n");
    }
}