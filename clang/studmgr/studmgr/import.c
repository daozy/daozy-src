/**
 * @file: import.c
 * 录入学员信息模块
 */

#include "import.h"
#include "global_def.h"
#include "store.h"

static int _import_one() {
    Student stud = { 0 };
    unsigned count = 0;
    unsigned i = 0;
    int ret = 0;

    printf("====请输入学员信息====\n");
    printf(" 姓名: ");
    scanf("%s", stud.name);
    printf(" 性别: ");
    scanf("%s", &stud.sex);
    printf(" 年龄: ");
    scanf("%u", &stud.age);
    printf(" 学号: ");
    scanf("%s", stud.id);
    printf(" 班级: ");
    scanf("%s", stud.class);

    printf("====下面进行课程录入====\n");
    printf(" 输入课程的数量: ");
    scanf("%u", &count);
    if (count > MAX_COURSE) {
        printf("课程最大数量 %u, 请重新输入\n", MAX_COURSE);
        return -1;
    }

    for (i = 0; i < count; i++) {
        printf("课程名: ");
        scanf("%s", stud.course[i].name);
        printf("分数: ");
        scanf("%u", &stud.course[i].score);
    }

    ret = import_one(&stud);

    if (ret == 0) {
        printf("保存成功!!!!\n");
    }
    
    return ret;
}

static int _import_from_file() {
    char path[MAX_PATH] = { 0 };
    int ret = 0;

    printf("请输入文件名: ");
    scanf("%s", path);


    if ((ret = import_file(path)) == 0) {
        printf("导入成功!!!!\n");
    }

    return ret;
}

void sm_import() {
    unsigned select = 0;
    printf("====请选择增加方式====\n");
    printf(" 1. 增加单个学员信息.\n");
    printf(" 2. 通过文件批量导入.\n");
    printf(" 请选择序号: ");
    scanf("%d", &select);
    if (select == 1) {
        _import_one();
    }
    else if (select == 2) {
        _import_from_file();
    }
    else {
        printf("[error] 选择错误.\n");
    }
}