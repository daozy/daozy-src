/**
 * @file: main.c
 * 学员管理系统
 * 1. 登录：用户名和密码进行校验。
 * 2. 菜单功能：登录、查询、修改、删除。
 * 3. 查询功能：通过用户名或者ID查询学员的相关信息。
 * 4. 学员信息的录入：姓名、性别、年龄、学号、班级、成绩、学员选择的课程。
 * 5. 删除学员记录：根据学号删除。
 * 6. 修改学员信息：查询到，然后修改。
 */

#include "global_def.h"
#include "login.h"
#include "import.h"
#include "search.h"
#include "delete.h"

// 菜单
unsigned menu();

int main() {
    unsigned select = 0;
    char q = '\0';
    
    if (login_face() != LOGIN) {
        printf("按 q 键系统退出\n");
        while ((q = getchar()) != 'q') {}
        return -1;
    }

    while (1) {
        select = menu();
        switch (select) {
            case M_SEARCH:
                sm_search();
                break;
            case M_IMPORT:
                sm_import();
                break;
            case M_MODIFY:
                printf("敬请期待，课后作业!!!\n");
                break;
            case M_DEL:
                sm_delete();
                break;
            case M_EXPORT:
                sm_export();
                break;
            case M_EXIT:
                return -1;
                break;
            default:
                printf("请选择正确序号!!!\n");
                break;
        }
    }

    return 0;
}

static unsigned menu() {
    unsigned select = 0;
    printf("========学员管理系统=======\n");
    printf("\t 1. 查询\n");
    printf("\t 2. 增加\n");
    printf("\t 3. 修改\n");
    printf("\t 4. 删除\n");
    printf("\t 5. 导出\n");
    printf("\t 6. 退出\n");

    printf("请选择功能序号(1~5)：");
    scanf("%d", &select);
    return select;
}