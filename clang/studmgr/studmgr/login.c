/**
 * @file: login.c
 * 登录功能实现
 */
#include "login.h"

// 默认用户名和密码
static const char* DEF_USER = "admin";
static const char* DEF_PWD = "admin";

static int login_auth(const char* user, const char* pwd) {
    if (strcmp(user, DEF_USER) != 0) {
        printf("用户名不存在\n");
        return -1;
    }

    if (strcmp(pwd, DEF_PWD) != 0) {
        printf("密码错误\n");
        return -2;
    }

    return 0;
}

LoginStatus login_face() {
    unsigned try_times = 0;
    char user[MAX_USER_SIZE] = { 0 };
    char pwd[MAX_PWD_SIZE] = { 0 };

    printf("========学员管理系统=======\n");
    printf("请用户输入用户名和密码进行登录\n");
    
    while (try_times < 3) {
        printf("用户名: ");
        scanf("%s", user);
        printf("密码: ");
        scanf("%s", pwd);
        if (login_auth(user, pwd) == 0) {
            printf("%s登录成功\n", user);
            break;
        }
        try_times++;
    }

    if (try_times == 3) {
        printf("登录失败，只能尝试3次登录\n");
        return LOGOUT;
    }

    return LOGIN;
}