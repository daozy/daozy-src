/**
 * @file: delete.c
 * 查询功能
 */
#include "delete.h"
#include "global_def.h"
#include "store.h"

static void _print(const Student studs[], const unsigned num) {
    unsigned i = 0;

    printf("---------------------------------------------------\n");
    printf("序号 | 姓名  |  性别  |  年龄  |  学号  |  班级 \n");
    for (i = 0; i < num; i++) {
        printf(" %u   | %-6s| %-6s | %-6u | %-6s | %-6s\n", i + 1, studs[i].name, studs[i].sex, studs[i].age, studs[i].id, studs[i].class);
    }
    printf("---------------------------------------------------\n");
}

static void _delete_by_cond() {
    Student stud;
    Student* results = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    unsigned count = 0;

    printf("====请输入删除的条件====\n");
    printf("注：如果不需要查询条件，字符串类型输入*\n");
    printf("注：整数类型不需要的，输入0\n");
    printf(" 姓名: ");
    scanf("%s", stud.name);
    printf(" 性别: ");
    scanf("%s", &stud.sex);
    printf(" 年龄: ");
    scanf("%u", &stud.age);
    printf(" 学号: ");
    scanf("%s", stud.id);
    printf(" 班级: ");
    scanf("%s", stud.class);

    count = del_by_keys(&stud, results, MAX_STUDS);
    printf("删除 %u 条记录.\n", count);
    if (count > 0) {
        _print(results, count);
    }

    free(results);
}

static void _delete_list() {
    Student* results = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    Student del_stud = { 0 };
    unsigned count = 0;
    unsigned del_idx = 0;

    count = find_list(results, MAX_STUDS);
    printf("查询到 %u 个结果.\n", count);
    if (count > 0) {
        _print(results, count);
    }

    printf("请选择删除序号: ");
    scanf("%u", &del_idx);
    if (del_idx < 1 || del_idx > count) {
        printf("选择的序号超出范围\n");
        free(results);
        return;
    }

    count = del_by_keys(&results[del_idx - 1], &del_stud, 1);
    printf("删除 %u 条记录.\n", count);
    if (count > 0) {
        _print(&del_stud, count);
    }

    free(results);
}

void sm_delete() {
    unsigned select = 0;

    printf("====请选择删除方式====\n");
    printf(" 1. 根据各种条件删除.\n");
    printf(" 2. 列出所有，选择序号删除.\n");
    printf(" 请选择序号：");
    scanf("%u", &select);

    if (select == 1) {
        _delete_by_cond();
    }
    else if (select == 2) {
        _delete_list();
    }
    else {
        printf("[error] 请输入正确的序号.\n");
    }
}