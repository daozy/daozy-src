/**
 * @file: login.h
 * 登录功能
 */
#pragma once

#include <stdio.h>
#include "global_def.h"

/**
 * @功能：实现登录界面, 最多错误3次就不允许再登录
 */
LoginStatus login_face();