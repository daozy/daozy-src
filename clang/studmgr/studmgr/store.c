/**
 * @file: store.c
 * 登录功能
 */
#include "store.h"

 /**
  * 写入接口
  */
static int _write(const Student* stud) {
    if (stud == NULL) {
        return -1;
    }

    FILE* fp = fopen(STORE_FILE, "ab");
    if (fp == NULL) {
        fp = fopen(STORE_FILE, "wb");
        if (fp == NULL) {
            return -1;
        }
    }

    if (fwrite(stud, sizeof(Student), 1, fp) != 1) {
        fclose(fp);
        return -1;
    }

    fclose(fp);

    return 0;
}

/**
 * 追加方式写入多个记录
 */
static int _write_mult(const char* path, const Student stud[], const unsigned num) {
    if (stud == NULL && num <= 0) {
        return -1;
    }

    FILE* fp = fopen(path, "ab");
    if (fp == NULL) {
        fp = fopen(path, "wb");
        if (fp == NULL) {
            return -1;
        }
    }

    if (fwrite(stud, sizeof(Student), num, fp) != num) {
        fclose(fp);
        return -1;
    }

    fclose(fp);

    return num;
}

/**
 * 重写文件，多个记录
 */
static int _rewrite_mult(const char* path, const Student stud[], const unsigned num) {
    FILE* fp = fopen(path, "wb");
    if (fp == NULL) {
        return -1;
    }

    if (stud != NULL && num > 0) {
        if (fwrite(stud, sizeof(Student), num, fp) != num) {
            fclose(fp);
            return -1;
        }
    }

    fclose(fp);

    return num;
}

/**
 * 读取接口
 */
static int _read(Student* stud) {
    unsigned count = 0;

    if (stud == NULL) {
        return -1;
    }

    FILE* fp = fopen(STORE_FILE, "rb");
    if (fp == NULL) {
        return -1;
    }

    count = fread(stud, sizeof(Student), 1, fp);

    fclose(fp);

    return count;
}

/**
 * 读取多个
 * 返回值：返回0，表示读完； 返回1, 表示未读完
 */
static int _read_mult(const char* path, Student stud[], const unsigned num) {
    unsigned count = 0;

    if (stud == NULL) {
        return -1;
    }

    FILE* fp = fopen(path, "rb");
    if (fp == NULL) {
        return -1;
    }

    count = fread(stud, sizeof(Student), num, fp);
    fclose(fp);

    return count;
}

/**
 * 导出
 * 返回值：返回0，表示读完； 返回1, 表示未读完
 */
static int _export_info(const Student stud[], const unsigned num) {
    char export_path[MAX_PATH] = { 0 };

    printf("请输入导出的文件名: ");
    scanf("%s", export_path);

    return _write_mult(export_path, stud, num);
}

static int _match(const Student* stud1, const Student* stud2) {
    if (strcmp(stud1->name, "*") != 0 && strcmp(stud1->name, stud2->name) != 0) {
        return -1;
    }

    if (strcmp(stud1->sex, "*") != 0 && strcmp(stud1->sex, stud2->sex) != 0) {
        return -1;
    }

    if (stud1->age != 0 && stud1->age != stud2->age) {
        return -1;
    }

    if (strcmp(stud1->id, "*") != 0 && strcmp(stud1->id, stud2->id) != 0) {
        return -1;
    }

    if (strcmp(stud1->class, "*") != 0 && strcmp(stud1->class, stud2->class) != 0) {
        return 1;
    }

    return 0;
}

/**
 * 导入单个学员信息
 */
int import_one(const Student* stud) {
    return _write(stud);
}

/**
 * 通过文件导入学员信息
 */
int import_file(const char* path) {
    Student* studs = (Student*)malloc(MAX_STUDS * sizeof(Student));
    unsigned count = 0;
    int ret = 0;

    if ((count = _read_mult(path, studs, MAX_STUDS)) <= 0) {
        printf("[error] 读取导入文件失败(%s).\n", path);
        return -1;
    }

    // studmgr_back.dat -> studmgr.dat
    ret = _write_mult(STORE_FILE, studs, count);
    free(studs);
    
    return ret;
}

int export_file(const char* path, const Student studs[], const unsigned num) {
    return _rewrite_mult(path, studs, num);
}

/**
 * 查询：根据姓名、学号、班级、年龄
 */
int find_by_keys(const Student* cond, Student results[], const unsigned num) {
    Student* studs = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    unsigned count = 0;
    unsigned i = 0;
    unsigned j = 0;

    count = _read_mult(STORE_FILE, studs, MAX_STUDS);
    for (i = 0; i < count; i++) {
        if (_match(cond, &studs[i]) != 0) {
            continue;
        }

        // 所有条件都满足
        results[j++] = studs[i];
    }

    free(studs);

    return j;
}

/**
 * 查询：列出所有
 */
int find_list(Student results[], const unsigned num) {
    return _read_mult(STORE_FILE, results, num);
}

/**
 * 删除：根据各自条件删除
 */
int del_by_keys(const Student* cond, Student results[], const unsigned num) {
    Student* studs = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    Student* new_studs = (Student*)malloc(sizeof(Student) * MAX_STUDS);
    unsigned count = 0;
    unsigned i = 0;
    unsigned j = 0;
    unsigned k = 0;

    count = _read_mult(STORE_FILE, studs, MAX_STUDS);
    for (i = 0; i < count; i++) {
        if (_match(cond, &studs[i]) == 0) {
            // 保存需要被删掉的记录
            results[j++] = studs[i];
        } else {
            // 保存删掉之后的记录
            new_studs[k++] = studs[i];
        }
    }

    free(studs);

    // 回写到文件
    if (_rewrite_mult(STORE_FILE, new_studs, k) != k) {
        printf("[error]删除失败\n");
        free(new_studs);
        return -1;
    }
    
    free(new_studs);

    return j;
}

/**
 * 修改：先查询出来再修改
 */
int alert_from_find() {
    return 0;
}