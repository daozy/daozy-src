#include <stdio.h>  

int main(void) {
	int a, b, c, d;

	a = 10;
	b = a++;  // ++在后，在此句结束前a依然等于10，所以b等于a++之前的值10
	c = ++a;    // 上一句结束后a=11, 此句先做a自增1，等于12，再赋值给c=12
	d = 10 * a++;  // ++在后，a自增再此句结束后，d=10*12=120
	printf("b，c，d：%d，%d，%d", b, c, d);

	return 0;
}