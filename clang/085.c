#include <stdio.h>

// const 常量

// 1. 修饰全局变量
const int a = 100;

// 2. 修饰局部变量
void test() {
	const int b = 10;
}

// 3. 修饰函数参数
int add(const int a, const int b) {
	return a + b;
}

const int fun(const int a, const int b) {
	return a + b;
}

int main() {
	int sum = 0;

	add(1, 2);
	
	sum = fun(1, 2);
	sum = 100;


	return 0;
}