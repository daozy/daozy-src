#include <stdio.h>

void normal_var(const int a, const int b) {
	// &&
	if (a && b) {
		printf("a && b ? true\n");
	} else {
		printf("a && b ? false\n");
	}
	
	if (a || b) {
		printf("a || b ? true\n");
	} else {
		printf("a || b ? false\n");
	}
	
	if (!(a && b)) {
		printf("!(a && b) ? true\n");
	} else {
		printf("!(a && b) ? false\n");
	}
}

void expr() {
	int score[100][2] = {0};
	int number = 0;
	int i = 0;
	int boy_pass_cnt = 0;
	int girl_pass_cnt = 0;
	int all_fail_cnt = 0;
	
	printf("Please the number: ");
	scanf("%d", &number);

	// boy == 0, girl == 1
	for (i = 0; i < number; i++) {
		scanf("%d%d", &score[i][0], &score[i][1]);
	}

	for (i = 0; i < number; i++) {
		if (score[i][0] == 0 && score[i][1] >= 60) {
			boy_pass_cnt++;
		} else if (score[i][0] == 1 && score[i][1] >= 60) {
			girl_pass_cnt++;
		} else {
			all_fail_cnt++;
		}
	}

	printf("boy_pass_cnt = %d\n", boy_pass_cnt);
	printf("girl_pass_cnt = %d\n", girl_pass_cnt);
	printf("all_fail_cnt= %d\n", all_fail_cnt);
}		

int main() {
	int a = 0;
	int b = 1;
	
	normal_var(a, b);
	expr();

	return 0;
}
