#include <stdio.h>

int main() {
	int d = 0;
	char c = 0;
	char buffer[1024] = { 0 };
	char* pb = NULL;
	FILE* fp1 = NULL;
	FILE* fp = fopen("test.txt", "r+");
	if (NULL == fp) {
		printf("open file failed\n");
		return -1;
	}

	// fgetc
	while ((c = fgetc(fp)) != EOF) {
		printf("fgetc = %c\n", c);
	}

	// fputc
	c = fputc('a', fp);
	if (c == EOF) {
		printf("error: write file failed\n");
		return -1;
	}

	printf("fputc = %c\n", c);

	fseek(fp, 0, SEEK_SET);
	pb = fgets(buffer, sizeof(buffer), fp);
	printf("fgets buffer = %s\n", buffer);
	printf("fgets pb = %s\n", pb);
	printf("pb=%p, buffer=%p\n", pb, buffer);

	printf("ret = %d\n", fputs("xxxxxx", fp));

	fseek(fp, 0, SEEK_SET);
	fscanf(fp, "%s%d%c", buffer, &d, &c);
	printf("buffer = %s, d = %d, c = %c\n", buffer, d, c);

	//fp1 = freopen("test1.txt", "w", fp);
	//fputs("YYYYYY\n", fp1);
	//fclose(fp1);

	if (feof(fp) == 0) {
		printf("left: %s\n", fgets(buffer, sizeof(buffer), fp));
	}
	else {
		printf("end of file\n");
	}

	while ((c = fgetc(fp)) != EOF) {
		printf("fgetc = %c\n", c);
	}

	if (ferror(fp) != 0) {
		perror(buffer);
		printf("error: %s\n", buffer);
		clearerr(fp);
	}

	return 0;
}