#include <stdio.h>
#include <string.h>

typedef char Name[50];
typedef char Type;

typedef char int8;
typedef short int16;
typedef int int32;
typedef long long int64;

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;

typedef int32 Count;
typedef uint32 Bid;

typedef struct {
	Name name;
	Type type;
	Count count;
	Bid book_id;
} Book;

int main() {
	Book book;
	memset(&book, 0, sizeof(book));

	printf("please input book's name: ");
	scanf("%s", book.name);
	printf("please input book's type: ");
	scanf("\n%c", &book.type);
	printf("please input book's count: ");
	scanf("%d", &book.count);
	printf("please input book's id: ");
	scanf("%u", &book.book_id);

	printf("name: %s, type: %c, count: %d, id: %u\n", book.name, book.type, book.count, book.book_id);

	return 0;
}
