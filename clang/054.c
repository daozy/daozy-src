#include <stdio.h>
#include <string.h>


void test_strcat() {
	char str1[100] = "12345";
	char* str2 = "678910";
	char* str3 = NULL;

	printf("=====test_strcat=====\n");
	printf("str1_addr = %p\n", str1);
	str3 = strcat(str1, str2);
	printf("str3_addr = %p, str1 = %s, str3 = %s\n", str3, str1, str3);
}

void test_strncat() {
	char str1[100] = "12345"; // 这里要足够长，可以容纳下str1+str2的内容
	char* str2 = "678910";
	char* str3 = NULL;

	printf("=====test_strncat=====\n");
	printf("str1_addr = %p\n", str1);
	str3 = strncat(str1, str2, 3);
	printf("str3_addr = %p, str1 = %s, str3 = %s\n", str3, str1, str3);
}

void test_strchr() {
	char* str = "aaaxbbbxcccxdddd";
	char* ret = NULL;

	printf("=====test_strchr=====\n");
	printf("str_addr: %p\n", str);
	ret = strchr(str, 'x');
	printf("ret_addr: %p, ret: %s\n", ret, ret);
}

void test_strcmp() {
	int ret = 0;
	printf("=====test_strcmp=====\n");
	printf("%s\n", (ret = strcmp("abcd", "abcd")) > 0 ? "str1 > str2" : ret < 0 ? "str1 < str2" : "st1 == str2");
}

void test_strcpy() {
	char st1[6] = "1111";
	char* ret = NULL;
	printf("=====test_strcpy=====\n");
	printf("str1_addr: %p\n", st1);
	ret = strcpy(st1, "22222");
	printf("ret_addr: %p, ret: %s\n", ret, ret);
}

void test_strpbrk() {
	char* str1 = "aa2aa111bbbb";
	char* ret = NULL;
	printf("=====test_strpbrk=====\n");
	printf("str1_addr: %p\n", str1);
	ret = strpbrk(str1, "31");
	printf("ret_addr: %p, ret: %s\n", ret, ret);
}

void test_strstr() {
	char* str1 = "aabbcc11cc";
	char* ret = NULL;
	printf("=====test_strstr=====\n");
	printf("str1_addr: %p\n", str1);
	ret = strstr(str1, "cc");
	printf("ret_addr: %p, ret: %s\n", ret, ret);
}

void test_strtok() {
	char str1[] = "aabb#11#22";
	char* ret = NULL;
	printf("=====test_strtok=====\n");
	printf("str1_addr: %p\n", str1);
	ret = strtok(str1, "#");
	do {
		printf("ret_addr: %p, ret: %s\n", ret, ret);
		ret = strtok(NULL, "#");
	} while (ret != NULL);
}

int main() {
	test_strcat();
	test_strncat();
	test_strchr();
	test_strcmp();
	test_strcpy();
	test_strpbrk();
	test_strstr();
	test_strtok();
	return 0;
}