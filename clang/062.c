#include <stdio.h>
#include <time.h>

void test_gmtime() {
	time_t t1; // ����
	struct tm* t = NULL;
	// struct tm *gmtime(const time_t *time);

	printf("====test_gmtime====\n");
	time(&t1);
	t = gmtime(&t1);
	printf("%d-%d-%d %d:%d:%d\n", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
}

void test_mktime() {
	time_t t1 = 0;
	struct tm* t = NULL;
	// struct tm *localtime(const time_t *time);
	// time_t mktime(struct tm *time);  // long long int
	printf("====test_mktime====\n");
	time(&t1);

	t = localtime(&t1);
	printf("%d-%d-%d %d:%d:%d\n", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
	printf("time_t = %lld\n", mktime(t));
}

// size_t strftime(char *str, size_t maxsize, const char *fmt, struct tm *time);
void test_strftime(const char *fmt) {
	char str[128] = { 0 };
	time_t t = 0;

	printf("====test_strftime====\n");
	size_t len = 0;
	
	time(&t);
	len = strftime(str, sizeof(str), fmt, localtime(&t));
	printf("len = %d, time: %s\n", len, str);
}

int main() {
	test_gmtime();
	test_mktime();
	test_strftime("%Y-%m-%d %X %p");
	test_strftime("%x %X");
	test_strftime("%j %W");
	return 0;
}